/* ************************************************************************** */
/** Descriptive File Name

  @Company
    Company Name

  @File Name
    filename.c

  @Summary
    Brief description of the file.

  @Description
    Describe the purpose of this file.
 */
/* ************************************************************************** */

#include <xc.h>
#include "mysimplelib.h"


void Timer2control(TimerStates_t T2state)
{
	switch(T2state){
		case TOn :
			T2CONbits.ON = 1;
			break;
			
		case TOff:
			T2CONbits.ON = 0;			
			break;
	}
	
	return;
}


/* *****************************************************************************
 End of File
 */
