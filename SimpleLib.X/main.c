/* 
 * File:   main.c
 * Author: pf
 *
 * Created on 09 July 2018, 21:36
 */

#include <xc.h>
#include "mysimplelib.h"

/*
 * 
 */
int main(int argc, char** argv) {

    /* Just a dummy call to Timer2Control */
    Timer2control(TOn);
    
    return (EXIT_SUCCESS);
}

