
#ifndef _MYSIMPLELIB_H    /* Guard against multiple inclusion */
#define _MYSIMPLELIB_H


/* Provide C++ Compatibility */
#ifdef __cplusplus
extern "C" {
#endif

    typedef enum TimerStates {
        TOn, TOff
    } TimerStates_t;

    void Timer2control(TimerStates_t T2state);



    /* Provide C++ Compatibility */
#ifdef __cplusplus
}
#endif

#endif /* _MYSMPLELIB_H */

/* *****************************************************************************
 End of File
 */
