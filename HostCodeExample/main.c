/*
 * Demo file for creating C programs using the PIC32 registers as defined by xc.h
 *
 * This demo uses a mem_map.c file that contains a mock to replicate the PIC32
 * special function registers in the PC memory. 
 *
 * mem_map.c and mem_map.h are created by the bash script mem_map_create.sh
 *
 * This example changes the ON and WDTCLR bits of WDTCON register, by
 * accessing the bits through the WDTCONbits structure and shows the  result
 * accessing WDTCON register.
 *
 * See the accompanying Makefile for compilation details. 
 *
 * pf@ua.pt, October 2015 
 */


#include <stdio.h>

#include "mem_map.h"					


int main(void)
{
	printf("Usage of XC32 include files for PC code.\n");
	printf("Using WDTCON register...\n\n");

	/* Set all bits to zero */
	WDTCON = 0x0000;

	printf("Setting ON bit:\n");

	/* and write to screen */
	printf("Initial value:\t0x%04x\n",WDTCON);

	/* Now, set a single bit */
	WDTCONbits.ON = 1;

	/* Check the result */
	printf("Final value:\t0x%04x\n",WDTCON);


	/* Now with the WDTCLR bit */
	printf("Setting WDTCLR bit:\n");

	/* Write the value to screen */
	printf("Initial value:\t0x%04x\n",WDTCON);

	/* set WDTCLR bit */
	WDTCONbits.WDTCLR = 1;

	/* Check the result */
	printf("Final value:\t0x%04x\n",WDTCON);

	return 0;

}
